package com.qindel.testEjercicio;


import io.restassured.RestAssured;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.ArrayList;
import static io.restassured.RestAssured.given;

public class AppendInsertDeleteModifyLinesTest {
    //mvn test -Dserver.port=8080 -Dserver.host=http://localhost

    String insertFileJson = "{\"name\": \"%s\"}"; // json to add a new at the end
    String appendLine = "{\"fileId\": %d,\"lineText\": \"%s\"}";
    String fullJsonLine = "{\"fileId\": %d,\"lineNumber\": %d,\"lineText\": \"%s\"}";

    String jsonMediaType = "application/json";
    String insertFileUri = "/file";
    String appendInsertLineUri =  "/line/create";
    String modifyLineUri =  "/line/modify";
    String deleteLineUri =  "/line/{file}/{line}";
    String undoUri = "/file/{file}/undo";
    String countLinesUri = "/file/{file}/countLines";
    String getFileLinesUri = "/file/{file}";


    @BeforeClass
    public static void setup() {
        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = Integer.valueOf(8080);
        }else{
            RestAssured.port = Integer.valueOf(port);
        }

        String baseHost = System.getProperty("server.host");
        if(baseHost==null){
            baseHost = "http://localhost";
        }
        RestAssured.baseURI = baseHost;

    }


    @Test
    public void generalWorkingTest() {
        ArrayList<String> status = new ArrayList();

        // add a file
        int fileId = given().contentType(jsonMediaType)
                .body(String.format(insertFileJson, "Harry Potter 1"))
                .when().post(insertFileUri)
                .then().statusCode(201).extract().body().as(Integer.class);

        int incorrectFileId = fileId + 3;
        int lineId = 0;
        System.out.println(String.format("trabajando con el archivo %d", fileId));

        countLines(fileId);
        status.add(getFileLinesAsString(fileId));

        // inserta una linea en una posicion mas alta de la que existe
        lineId = given().contentType(jsonMediaType)
                .body(String.format(fullJsonLine, fileId, 5, "Erase una vez"))
                .when().post(appendInsertLineUri)
                .then().statusCode(201).extract().body().as(Integer.class);

        Assert.assertEquals(1, lineId);
        countLines(fileId);
        status.add(getFileLinesAsString(fileId));

        // añade una linea al final
        lineId = given().contentType(jsonMediaType)
                .body(String.format(appendLine, fileId, "un niño que tenia magia"))
                .when().post(appendInsertLineUri)
                .then().statusCode(201).extract().body().as(Integer.class);

        Assert.assertEquals(2, lineId);
        countLines(fileId);
        status.add(getFileLinesAsString(fileId));

        // inserta una linea al principio
        lineId = given().contentType(jsonMediaType)
                .body(String.format(fullJsonLine,fileId,1, "Me cuelo la primera"))
                .when().post(appendInsertLineUri)
                .then().statusCode(201).extract().body().as(Integer.class);

        Assert.assertEquals(1, lineId);
        countLines(fileId);
        status.add(getFileLinesAsString(fileId));

        // modifica la segunda linea
        given().contentType(jsonMediaType)
                .body(String.format(fullJsonLine,fileId,2, "Me han modificado"))
                .when().put(modifyLineUri)
                .then().statusCode(204);

        countLines(fileId);
        status.add(getFileLinesAsString(fileId));

        // borra la segunda linea
        given().pathParam("file", fileId).pathParam("line", 1)
                .when().delete(deleteLineUri)
                .then().statusCode(204);

        countLines(fileId);

        // aplicar undo hasta llegar al principio
        for(int i = status.size(); i >0; i--){
            given().pathParam("file", fileId)
                    .when().put(undoUri)
                    .then().statusCode(204);

            countLines(fileId);
            Assert.assertEquals(getFileLinesAsString(fileId), status.get(i-1));
        }

        // aplica un undo mas para ver que esta vacio y falla
        given().pathParam("file", fileId)
                .when().put(undoUri)
                .then().statusCode(404);
    }

    @Test
    public void generalErrorTest(){
        // add a file
        int fileId = given().contentType(jsonMediaType)
                .body(String.format(insertFileJson, "Harry Potter 1"))
                .when().post(insertFileUri)
                .then().statusCode(201).extract().body().as(Integer.class);

        int incorrectFileId = fileId + 3;

        // aplica un undo a un archivo que no existe en base de datos
        given().pathParam("file", incorrectFileId)
                .when().put(undoUri)
                .then().statusCode(404);

        // inserta una linea en un fichero que no existe
        given().contentType(jsonMediaType)
                .body(String.format(fullJsonLine,incorrectFileId,1, "Me cuelo la primera"))
                .when().post(appendInsertLineUri)
                .then().statusCode(404);

        // inserta una linea en la posicion 0 espera bad request
        given().contentType(jsonMediaType)
                .body(String.format(fullJsonLine,fileId,0, "Me cuelo la primera"))
                .when().post(appendInsertLineUri)
                .then().statusCode(400);
    }

    private void countLines(int file){
        String body = given().pathParam("file", file)
                .when().get(countLinesUri)
                .then().statusCode(200)
                .extract().body().as(String.class);
        System.out.println(body);
    }

    private String getFileLinesAsString(int file){
        String body = given().pathParam("file", file)
                .when().get(getFileLinesUri)
                .then().statusCode(200)
                .extract().body().asString();
        System.out.println(body);
        return body;
    }


}