# testAssuredEjercicioQindel

Test de Rest Assured para el proyecto Ejercicio Qindel, de manera que se pueda comprobar mediante una herramienta externa el correcto funcionamiento del proyecto.

El proyecto que testea esta alojado aqui: https://bitbucket.org/nachoteam/ejercicio-qindel/src/master/

# Prueba

Para probarlo hay que descargarse el codigo mediante git clone y ejecutar mvn test.

Para que el test sea correcto, es necesario que el proyecto se haya ejecutado antes.

Por defecto esta configurado el test en localhost y en el puerto 8080. 

En caso de querer cambiarlo, se deberia ejecutar mvn test -Dserver.port=8080 -Dserver.host=http://localhost, configurando estas propiedades debidamente.